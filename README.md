### Getting started ###
1. `npm install`

### Watching files ###
1. `npm run watch`

### How to create build ###
1. `npm run dist`

### Update package to remote npm ###
1. `npm run release` (create css and dist folder)
2. `git add .`
3. `npm run commit`
4. `npm version patch | minor | major` (under the hood npm runs a new commit with updated version)
5. `git push`
6. `npm publish`

